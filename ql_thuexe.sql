-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 28, 2019 at 12:06 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ql_thuexe`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `username` char(50) NOT NULL,
  `password` text NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` text NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`username`, `password`, `name`, `phone`, `address`, `type`) VALUES
('admin', '123456', 'Admin', '0123456789', 'Ha Nội', 0),
('test', '123456', 'Test', '0123456789', 'Hà ', 1),
('test1', '1', 'Test', '0123456789', 'Hà Nội', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `order_date` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `username` char(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `order_date`, `status`, `username`) VALUES
(2, '05:23 25/05/2019', -1, '0'),
(3, '05:40 25/05/2019', -1, 'test'),
(4, '05:48 25/05/2019', 3, 'test'),
(5, '06:26 25/05/2019', -1, 'test'),
(6, '06:41 25/05/2019', -1, 'test'),
(7, '11:08 28/09/2019', 3, 'test1'),
(8, '04:33 28/09/2019', 0, 'test1'),
(9, '04:39 28/09/2019', 0, 'test1');

-- --------------------------------------------------------

--
-- Table structure for table `cart_detail`
--

CREATE TABLE `cart_detail` (
  `id_cart` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `size` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart_detail`
--

INSERT INTO `cart_detail` (`id_cart`, `id_item`, `count`, `price`, `size`) VALUES
(1, 1, 4, 600000, ''),
(1, 2, 3, 360000, ''),
(2, 1, 1, 150000, ''),
(2, 2, 1, 120000, ''),
(3, 1, 1, 150000, ''),
(3, 2, 1, 120000, ''),
(4, 1, 1, 150000, ''),
(5, 1, 1, 750000, ''),
(6, 1, 10, 7500000, ''),
(6, 2, 1, 600000, ''),
(7, 1, 1, 150000, ''),
(8, 1, 1, 150000, ''),
(8, 1, 1, 150000, ''),
(8, 2, 3, 360000, ''),
(8, 2, 3, 360000, ''),
(9, 2, 1, 120000, 'S'),
(9, 2, 2, 240000, 'M');

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `name`) VALUES
(6, ' ĐỒ BÓNG ĐÁ'),
(7, 'ĐỒ TẬP GYM'),
(8, 'ĐỒ TẬP YOGA'),
(9, 'PHỤ KIỆN TẬP YOGA');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `url` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `id_item`, `url`) VALUES
(18, 0, 'img/item/s'),
(19, 0, 'img/item/screencapture-localhost-82-dichvucong-kethon-php-2019-05-19-22_47_41.png'),
(20, 0, 'img/item/screencapture-localhost-82-dichvucong-khaisinh-php-2019-05-19-22_47_34.png'),
(21, 0, 'img/item/screencapture-localhost-82-dichvucong-khaitu-php-2019-05-19-22_47_58.png'),
(37, 1, 'img/item/Ao-ac-milan-san-nha-1-1-600x600.jpg'),
(38, 1, 'img/item/Ao-ac-milan-san-nha-2-1-600x600.jpg'),
(39, 2, 'img/item/Ao-arsenal-san-nha-1-1-600x600.jpg'),
(40, 2, 'img/item/Ao-arsenal-san-nha-2-1-600x600.jpg'),
(41, 3, 'img/item/Ao-barca-mau-ba-1.png'),
(42, 3, 'img/item/Ao-barca-mau-ba-2-300x300.png');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `producer` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `desc` text COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL,
  `id_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`name`, `producer`, `price`, `desc`, `id`, `id_group`) VALUES
('Áo AC Milan 2019 – 2020 sọc đỏ đe', 'AC Milan', 150000, '<p>&nbsp;</p>\r\n\r\n<p>C&oacute; mặt tại thị trường Việt Nam từ năm 2009 Wave S 110 lu&ocirc;n được người ti&ecirc;u d&ugrave;ng đ&oacute;n nhận nồng nhiệt. Nhằm đến đối tượng kh&aacute;ch h&agrave;ng l&agrave; đại đa số người ti&ecirc;u d&ugrave;ng Việt Nam, Honda đ&atilde; kh&ocirc;ng ngừng cải tiến Wave S với mong muốn mang đến cho kh&aacute;ch h&agrave;ng một chiếc xe với nhiều t&iacute;nh năng tiện dụng nhưng kh&ocirc;ng k&eacute;m phần tinh tế v&agrave; thời trang.</p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th colspan=\"1\" rowspan=\"1\">\r\n			<p><strong>&Aacute;o AC Milan 2019 &ndash; 2020</strong>&nbsp;m&ugrave;a giải mới năm sau đ&atilde; được h&atilde;ng thể thao Puma giới thiệu với người h&acirc;m mộ Rossoneri v&agrave;o cuối m&ugrave;a giải vừa qua. Với thiết kế đơn giản nhưng mang đậm n&eacute;t thể thao hơn.</p>\r\n\r\n			<p><img alt=\"Áo ac milan sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-ac-milan-san-nha-0.jpg\" style=\"height:620px; width:900px\" /></p>\r\n\r\n			<h2>Chi tiết &aacute;o AC Milan 2019 &ndash; 2020 sọc đỏ đen s&acirc;n nh&agrave;:</h2>\r\n\r\n			<p>Năm nay thiết kế của &aacute;o b&oacute;ng đ&aacute; s&acirc;n nh&agrave; AC Milan đ&atilde; được đơn giản v&agrave; năng động, thể thao hơn với m&ugrave;a giải 2018 &ndash; 2019 trước đ&oacute;. C&aacute;c đường sọc đỏ v&agrave; đen được thiết kế nhỏ hơn, mang đậm n&eacute;t truyền thống l&acirc;u đời tr&ecirc;n c&aacute;c &aacute;o đấu s&acirc;n nh&agrave; từ trước đến nay của nữa đỏ th&agrave;nh Milan.</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><img alt=\"Áo ac milan sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2018/07/Ao-ac-milan-san-nha-1-400x400.jpg\" style=\"height:400px; width:400px\" /><img alt=\"Áo milan sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-ac-milan-san-nha-1-1-400x400.jpg\" style=\"height:400px; width:400px\" /></p>\r\n\r\n			<p>B&ecirc;n dưới cổ &aacute;o đấu được in một biểu tượng Con quỹ đỏ nhỏ b&ecirc;n dưới, tạo n&ecirc;n điểm nhấn cho mẫu &aacute;o nằm nay.</p>\r\n\r\n			<p><img alt=\"Áo ac milan sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-ac-milan-san-nha-3-400x400.jpg\" style=\"height:400px; width:400px\" /><img alt=\"Áo ac milan sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-ac-milan-san-nha-4-400x400.jpg\" style=\"height:400px; width:400px\" /></p>\r\n\r\n			<h2>H&igrave;nh ảnh &aacute;o đấu AC Milan s&acirc;n nh&agrave; m&ugrave;a giải 2019 &ndash; 2020:</h2>\r\n\r\n			<p><img alt=\"Áo ac milan sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-ac-milan-san-nha-5-800x800.jpg\" style=\"height:800px; width:800px\" /></p>\r\n			</th>\r\n			<th colspan=\"1\" rowspan=\"1\">&nbsp;</th>\r\n		</tr>\r\n	</thead>\r\n</table>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th colspan=\"1\" rowspan=\"1\">&nbsp;</th>\r\n			<th colspan=\"1\" rowspan=\"1\">&nbsp;</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 6),
('Áo Arsenal đỏ sân nhà 2019 – 2020', 'Arsenal ', 120000, '<p>Đầu th&aacute;ng 7, H&atilde;ng thể thao Đức Adidas đ&atilde; giới thiệu với người h&acirc;m mộ b&oacute;ng đ&aacute; th&agrave;nh London mẫu&nbsp;<strong>&aacute;o Arsenal s&acirc;n nh&agrave;</strong>&nbsp;2019 &ndash; 2020. H&atilde;ng h&agrave;ng kh&ocirc;ng Fly Emirates v&agrave; h&atilde;ng thể thao Adidas sẽ l&agrave; nh&agrave; t&agrave;i trợ ch&iacute;nh cho The Gunners năm nay.</p>\r\n\r\n<p><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-0.jpg\" style=\"height:506px; width:900px\" /></p>\r\n\r\n<h2>Chi tiết &aacute;o Arsenal đỏ s&acirc;n nh&agrave; 2019 &ndash; 2020</h2>\r\n\r\n<p>Với sự thay đổi nh&agrave; t&agrave;i trợ Adidas sau khi kết th&uacute;c hợp đồng c&ugrave;ng Puma, mẫu thiết kế của &aacute;o đấu năm nay c&oacute; n&eacute;t chung của c&aacute;c mẫu &aacute;o đấu của Adidas t&agrave;i trợ. Ba sọc tr&ecirc;n vai &aacute;o v&agrave; cổ &aacute;o tr&aacute;i tim, sử dụng gam m&agrave;u đỏ &ndash; trắng.</p>\r\n\r\n<p><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2018/07/Ao-arsenal-san-nha-1-400x400.jpg\" style=\"height:400px; width:400px\" /><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-1-1-400x400.jpg\" style=\"height:400px; width:400px\" /></p>\r\n\r\n<h2>H&igrave;nh ảnh &aacute;o đ&aacute; b&oacute;ng Arsenal s&acirc;n nh&agrave; 2019 &ndash; 2020</h2>\r\n\r\n<p><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-6.jpg\" style=\"height:1160px; width:800px\" /><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-5.jpg\" style=\"height:1160px; width:800px\" /><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-4.jpg\" style=\"height:1160px; width:800px\" /><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-10.jpg\" style=\"height:1160px; width:800px\" /><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-8.jpg\" style=\"height:1160px; width:800px\" /><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-7.jpg\" style=\"height:1160px; width:800px\" /><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-9.jpg\" style=\"height:1160px; width:800px\" /><img alt=\"Áo arsenal sân nhà\" src=\"https://www.sporter.vn/wp-content/uploads/2017/06/Ao-arsenal-san-nha-3.jpg\" style=\"height:1160px; width:800px\" /></p>\r\n', 2, 6),
('Áo Barca sân khách 2019 – 2020 mẫu 3', 'Barca', 100000, '<p>Bạn c&oacute; thể đặt c&aacute;c mẫu quần &aacute;o b&oacute;ng đ&aacute; tại Sporter.vn với gi&aacute; lu&ocirc;n mềm nhất, ch&uacute;ng t&ocirc;i chuy&ecirc;n cung cấp c&aacute;c mẫu quần &aacute;o thi đấu b&oacute;ng đ&aacute; CLB &nbsp;m&ugrave;a 2019 &ndash; 2020 mới nhất với c&aacute;c loại sau:</p>\r\n\r\n<h2>&Aacute;o b&oacute;ng đ&aacute; Barcelona s&acirc;n kh&aacute;ch mẫu ba 2019 &ndash; 2020 h&agrave;ng Th&aacute;i Lan F1:</h2>\r\n\r\n<p><strong>Gi&aacute; thị trường: 220.000đ/1 &aacute;o &ndash; 290.000đ/ 1 bộ.</strong><br />\r\n<strong>Gi&aacute; tại Sporter.vn: 195.000đ/ 1 &aacute;o &ndash; 260.000đ/ 1 bộ.<br />\r\nĐặt từ 10 bộ trở l&ecirc;n chỉ c&ograve;n 250.000đ/ 1 bộ</strong></p>\r\n\r\n<p><strong>Phi&ecirc;n bản bodyfit sẽ c&oacute; gi&aacute; cao hơn đ&ocirc;i ch&uacute;t so với phi&ecirc;n bản th&ocirc;ng thường.</strong></p>\r\n\r\n<p>&ndash; Đ&acirc;y l&agrave; mẫu &aacute;o đấu đẹp nhất v&agrave; gần giống nhất so với &aacute;o ch&iacute;nh h&atilde;ng, giống tới 99%, thường đường gọi l&agrave; &aacute;o F1 hay &aacute;o SF . Đ&acirc;y l&agrave; mẫu &aacute;o được may bằng vải Th&aacute;i Lan chất lượng cao cấp giống hệt vải ch&iacute;nh h&atilde;ng.<br />\r\n&ndash; Ưu điểm: Đẹp giống hệt h&agrave;ng ch&iacute;nh h&atilde;ng, nếu kh&ocirc;ng r&agrave;nh sẽ rất kh&oacute; ph&acirc;n biệt đ&acirc;u l&agrave; &aacute;o ch&iacute;nh h&atilde;ng, đ&acirc;u l&agrave;&nbsp;<a href=\"https://www.sporter.vn/ao-bong-da-thai-lan/\"><strong>&aacute;o Th&aacute;i Lan</strong></a>&nbsp;F1. Chất lượng cao, mặc m&aacute;t mẻ v&agrave; dễ chịu, thoải m&aacute;i khi mặc. Rẻ hơn rất nhiều so với h&agrave;ng ch&iacute;nh h&atilde;ng.<br />\r\n&ndash; Nhược điểm: Gi&aacute; cao hơn c&aacute;c mẫu kh&aacute;c kh&aacute; nhiều.</p>\r\n\r\n<h2>&Aacute;o b&oacute;ng đ&aacute; Barcelona s&acirc;n kh&aacute;ch mẫu ba 2019 &ndash; 2020 h&agrave;ng Việt Nam:</h2>\r\n\r\n<p><strong>Gi&aacute; thị trường: 100.000đ/1 bộ.</strong><br />\r\n<strong>Gi&aacute; tại Sporter.vn: 90.000đ 1 bộ.<br />\r\nĐặt từ 10 bộ trở l&ecirc;n chỉ c&ograve;n 85.000đ/ 1 bộ</strong></p>\r\n\r\n<p>&ndash; Đ&acirc;y l&agrave; mẫu quần &aacute;o c&acirc;u lạc bộ đang được b&aacute;n nhiều nhất tr&ecirc;n thị trường, thường được may bằng vải thun lạnh. Đ&acirc;y l&agrave; h&agrave;ng phổ th&ocirc;ng nhất hiện nay.<br />\r\n&ndash; Ưu điểm: Gi&aacute; rẻ, mặc cũng kh&aacute; bền.<br />\r\n&ndash; Nhược điểm: Mặc n&oacute;ng v&agrave; thấm h&uacute;t mồ h&ocirc;i kh&ocirc;ng tốt lắm. Chỉ giống khoảng 90% so với h&agrave;ng ch&iacute;nh h&atilde;ng.</p>\r\n', 3, 6);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pub_date` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`title`, `desc`, `content`, `image`, `pub_date`, `id`) VALUES
('4 bài tập yoga cho khuôn mặt thon gọn, trẻ hóa từ giáo viên người Nhật', 'Giáo viên người Nhật chia sẻ 4 bài tập yoga cho mặt, giúp trẻ hóa làn da dù đã bước qua tuổi lão hóa', '<p>Fumiko Takatsu (49 tuổi) l&agrave; một nữ gi&aacute;o vi&ecirc;n người Nhật đ&atilde; s&aacute;ng tạo ra những b&agrave;i tập yoga gi&uacute;p trẻ h&oacute;a l&agrave;n da tr&ecirc;n khu&ocirc;n mặt của m&igrave;nh. Trước đ&oacute;, c&ocirc; Fumiko chia sẻ:&nbsp;&quot;Năm 36 tuổi, t&ocirc;i đ&atilde; gặp một tai nạn xe hơi nghi&ecirc;m trọng v&agrave; n&oacute; cũng l&agrave;m ảnh hưởng đến l&agrave;n da của t&ocirc;i. Khu&ocirc;n mặt của t&ocirc;i trở n&ecirc;n m&eacute;o m&oacute;, da chảy xệ, xuất hiện nhiều nếp nhăn. T&ocirc;i v&ocirc; c&ugrave;ng hoảng loạn v&agrave; đ&atilde; t&igrave;m đủ mọi loại kem dưỡng da đắt tiền để cải thiện nhưng chẳng hiệu quả l&agrave; mấy. Sau đ&oacute;, t&ocirc;i đ&atilde; thử tập một số b&agrave;i tập yoga tr&ecirc;n khu&ocirc;n mặt của m&igrave;nh&quot;.</p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/11/photo-1-15627775898751686278902-0939.png\" style=\"height:502px; width:900px\" /><img alt=\"\" src=\"https://bs.serving-sys.com/serving/adServer.bs?cn=display&amp;c=19&amp;mc=imp&amp;pli=28970257&amp;PluID=0&amp;ord=1569591517998&amp;rtu=-1\" style=\"height:0px; width:0px\" /></p>\r\n\r\n<ul>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n</ul>\r\n\r\n<p><em>C&ocirc; Fumiko năm 36 tuổi v&agrave; khi ở độ tuổi 49, l&agrave;n da c&oacute; sự thay đổi r&otilde; rệt.</em></p>\r\n\r\n<p>Chỉ sau v&agrave;i tuần tập luyện, c&ocirc; Fumiko nhận thấy da mặt của m&igrave;nh đ&atilde; c&oacute; sự cải thiện t&iacute;ch cực. Tr&ecirc;n thực tế, yoga cũng l&agrave; một trong những bộ m&ocirc;n gi&uacute;p tăng cường sức khỏe, cải thiện v&oacute;c d&aacute;ng, nhan sắc v&agrave; giữ m&atilde;i sự tươi trẻ của l&agrave;n da thay v&igrave; phải đầu tư nhiều cho c&aacute;c phương ph&aacute;p thẩm mỹ.</p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/11/photo-2-15627775933071735616862-0939.jpg\" style=\"height:506px; width:900px\" /></p>\r\n\r\n<p>Qua đ&acirc;y, c&ocirc; Fumiko cũng chia sẻ 4 b&agrave;i tập yoga cho khu&ocirc;n mặt gi&uacute;p trẻ h&oacute;a l&agrave;n da của m&igrave;nh như sau.</p>\r\n\r\n<p><strong>B&agrave;i tập 1:</strong></p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/11/1-0942.png\" style=\"height:491px; width:587px\" /></p>\r\n\r\n<p>*C&aacute;ch thực hiện:</p>\r\n\r\n<p>- Bạn b&oacute;p tay lại th&agrave;nh 2 nắm đấm rồi đặt l&ecirc;n giữa tr&aacute;n.</p>\r\n\r\n<p>- Tiếp đ&oacute;, bạn miết nhẹ hai nắm tay từ giữa tr&aacute;n xuống hai b&ecirc;n th&aacute;i dương.</p>\r\n\r\n<p>- Kết th&uacute;c bằng c&aacute;ch ấn nhẹ hai nắm tay v&agrave;o hai b&ecirc;n th&aacute;i dương.</p>\r\n\r\n<p>Lặp lại động t&aacute;c n&agrave;y 4 lần nhịp nh&agrave;ng.</p>\r\n\r\n<p>*Lợi &iacute;ch:&nbsp;B&agrave;i tập n&agrave;y sẽ gi&uacute;p ngăn ngừa nếp nhăn tại v&ugrave;ng tr&aacute;n bằng c&aacute;ch thư gi&atilde;n c&aacute;c cơ quanh tr&aacute;n. Đồng thời, n&oacute; cũng gi&uacute;p loại bỏ căng thẳng hiệu quả.</p>\r\n\r\n<p><strong>B&agrave;i tập 2:</strong></p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/11/2-0942.png\" style=\"height:511px; width:545px\" /></p>\r\n\r\n<p>*C&aacute;ch thực hiện:</p>\r\n\r\n<p>- Bạn m&iacute;m chặt m&ocirc;i sang một b&ecirc;n (cảm nhận r&otilde; v&ugrave;ng m&aacute; đang căng l&ecirc;n), vừa m&iacute;m m&ocirc;i vừa ngước mặt hướng sang b&ecirc;n tr&aacute;i một g&oacute;c 45 độ.</p>\r\n\r\n<p>- Giữ tư thế n&agrave;y trong 3 gi&acirc;y v&agrave; lặp lại với hướng nh&igrave;n b&ecirc;n phải.</p>\r\n\r\n<p>Lặp lại động t&aacute;c n&agrave;y 4 lần nhịp nh&agrave;ng.</p>\r\n\r\n<p>*Lợi &iacute;ch:&nbsp;B&agrave;i tập n&agrave;y sẽ gi&uacute;p ngăn ngừa nguy cơ chảy xệ da mặt hiệu quả.</p>\r\n\r\n<p><strong>B&agrave;i tập 3:</strong></p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/11/3-0942.png\" style=\"height:477px; width:571px\" /></p>\r\n\r\n<p>*C&aacute;ch thực hiện:</p>\r\n\r\n<p>- Đặt hai b&agrave;n tay l&ecirc;n th&aacute;i dương, vừa đứng chu m&ocirc;i th&agrave;nh h&igrave;nh chữ O vừa k&eacute;o hai th&aacute;i dương nhẹ l&ecirc;n tr&ecirc;n.</p>\r\n\r\n<p>- Giữ tư thế n&agrave;y trong 2 gi&acirc;y.</p>\r\n\r\n<p>Lặp lại động t&aacute;c n&agrave;y 4 lần nhịp nh&agrave;ng.</p>\r\n\r\n<p>*Lợi &iacute;ch: B&agrave;i tập n&agrave;y sẽ gi&uacute;p l&agrave;m giảm bớt c&aacute;c nếp gấp ở v&ugrave;ng mũi, giảm nếp nhăn ở quanh miệng.</p>\r\n\r\n<p><strong>B&agrave;i tập 4:</strong></p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/11/5-0942.png\" style=\"height:443px; width:561px\" /></p>\r\n\r\n<p>*C&aacute;ch thực hiện:</p>\r\n\r\n<p>- Đặt ng&oacute;n tay trỏ v&agrave; ng&oacute;n tay giữa của b&agrave;n tay tr&aacute;i l&ecirc;n v&ugrave;ng th&aacute;i dương b&ecirc;n phải.</p>\r\n\r\n<p>- Tiếp theo, bạn d&ugrave;ng lực k&eacute;o nghi&ecirc;ng đầu về hướng b&ecirc;n tr&aacute;i.</p>\r\n\r\n<p>- Lặp lại với hướng b&ecirc;n phải.</p>\r\n\r\n<p>Lặp lại động t&aacute;c n&agrave;y 4 lần nhịp nh&agrave;ng.</p>\r\n\r\n<p>*Lợi &iacute;ch:&nbsp;B&agrave;i tập n&agrave;y sẽ gi&uacute;p n&acirc;ng m&iacute; mắt v&agrave; ngăn ngừa nguy cơ da chảy xệ.</p>\r\n', 'img/news/photo-2-15627775933071735616862-0939.jpg', '27/09/2019', 9),
('Các động tác yoga cho dáng đẹp, trẻ lâu ai cũng nên tập', 'Muốn trẻ lâu, vóc dáng thon gọn không khó chỉ cần bắt tay luyện tập yoga mỗi ngày', '<p>Yoga l&agrave; phương ph&aacute;p r&egrave;n luyện&nbsp;sức khỏe&nbsp;hiệu quả đ&atilde; c&oacute; từ h&agrave;ng ng&agrave;n năm trước ở Ấn Độ. Bản th&acirc;n từ &ldquo;yoga&rdquo; c&oacute; nghĩa l&agrave; &ldquo;gắn kết&rdquo; v&agrave; &ldquo;gắn kết với th&aacute;nh thần&rdquo;. Yoga gi&uacute;p cho con người sống t&iacute;ch cực, thanh thản v&agrave; b&igrave;nh y&ecirc;n. Tuy nhi&ecirc;n, c&aacute;i kh&oacute; nhất của nhiều người khi&nbsp;tập Yoga&nbsp;l&agrave; đ&atilde; kh&ocirc;ng gắn kết phần linh hồn v&agrave; sự tập trung cần c&oacute; của Yoga. Thế n&ecirc;n hầu hết mọi người đều nghĩ rằng, Yoga chỉ l&agrave; tổ hợp những động t&aacute;c uốn dẻo tay ch&acirc;n v&agrave; to&agrave;n bộ cơ thể.</p>\r\n\r\n<p>Tr&ecirc;n thực tế, Yoga thực sự c&ograve;n mang lại rất nhiều lợi &iacute;ch hơn một cơ thể mềm dẻo linh hoạt v&agrave; khỏe mạnh. Mọi người ở mọi độ tuổi v&agrave; t&igrave;nh trạng sức khỏe kh&aacute;c nhau đều c&oacute; thể luyện tập Yoga v&agrave; điều chỉnh để ph&ugrave; hợp với nhu cầu v&agrave; mục đ&iacute;ch ri&ecirc;ng của m&igrave;nh. Dưới đ&acirc;y l&agrave; một số động t&aacute;c yoga m&agrave; bạn c&oacute; thể tham khảo.</p>\r\n\r\n<p><strong>Động t&aacute;c con bướm</strong></p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/10/khi-cong-2138.jpg\" style=\"height:366px; width:640px\" /><img alt=\"\" src=\"https://bs.serving-sys.com/serving/adServer.bs?cn=display&amp;c=19&amp;mc=imp&amp;pli=28970257&amp;PluID=0&amp;ord=1569591583603&amp;rtu=-1\" style=\"height:0px; width:0px\" /></p>\r\n\r\n<ul>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n</ul>\r\n\r\n<p>Ngồi thẳng, hai ch&acirc;n gập lại, đầu gối mở sang hai b&ecirc;n, l&ograve;ng b&agrave;n ch&acirc;n &uacute;p v&agrave;o nhau, giữ cho cổ v&agrave; lưng thẳng, hai tay nắm lấy hai b&agrave;n ch&acirc;n. Giữ tư thế trong 1 &ndash; 3 ph&uacute;t.</p>\r\n\r\n<p>Động t&aacute;c n&agrave;y gi&uacute;p thư gi&atilde;n v&ugrave;ng cơ h&ocirc;ng, m&ocirc;ng, tăng cường khả năng vận động của&nbsp;khớp h&ocirc;ngv&agrave; ổn định chu kỳ kinh nguyệt.</p>\r\n\r\n<p><strong>Động t&aacute;c vặn người</strong></p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/10/5-dong-tac-danh-cho-nhung-nguoi-bi-mat-ngu-1-2138.jpg\" style=\"height:567px; width:850px\" /></p>\r\n\r\n<p>Ngồi khoanh ch&acirc;n, hai tay để l&ecirc;n đầu gối, giữ lưng v&agrave; cổ thẳng. Vặn xoay người sang một b&ecirc;n, tay c&ugrave;ng b&ecirc;n đưa ra ph&iacute;a sau lưng, tay c&ograve;n lại đặt l&ecirc;n đầu gối b&ecirc;n đối diện. H&iacute;t v&agrave;o, thở ra đều đặn trong 20 gi&acirc;y rồi quay trở lại tư thế chuẩn bị. Lặp lại ph&iacute;a đối diện.</p>\r\n\r\n<p>Động t&aacute;c n&agrave;y gi&uacute;p thư gi&atilde;n v&ugrave;ng lưng, cải thiện chức năng&nbsp;hệ ti&ecirc;u h&oacute;a&nbsp;v&agrave; giảm số đo v&ograve;ng eo.</p>\r\n\r\n<p><strong>Động t&aacute;c g&aacute;c ch&acirc;n l&ecirc;n tường</strong></p>\r\n\r\n<p>Nằm s&aacute;t v&agrave;o tường, hai ch&acirc;n duỗi thẳng l&ecirc;n tường, hai tay đưa sang hai b&ecirc;n vai. H&iacute;t thở s&acirc;u trong 3 &ndash; 5 ph&uacute;t.</p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/10/20160805-104051-legs-up-pose_600x481-2138.jpg\" style=\"height:481px; width:600px\" /></p>\r\n\r\n<p>Động t&aacute;c n&agrave;y gi&uacute;p tăng cường lưu th&ocirc;ng m&aacute;u l&ecirc;n c&aacute;c cơ quan ở nửa th&acirc;n tr&ecirc;n, giảm sưng ch&acirc;n, k&iacute;ch th&iacute;ch c&aacute;c cơ quan đường ruột v&agrave; cải thiện t&acirc;m trạng hiệu quả.</p>\r\n\r\n<p><strong>Động t&aacute;c cầu nguyện</strong></p>\r\n\r\n<p>Ngồi quỳ, hai đầu gối gập, hai b&agrave;n ch&acirc;n để s&aacute;t hai b&ecirc;n h&ocirc;ng, hai l&ograve;ng b&agrave;n tay &uacute;p v&agrave;o nhau để trước ngực, duỗi thẳng cổ v&agrave; lưng. Giữ tư thế, h&iacute;t thở s&acirc;u trong 1 ph&uacute;t.</p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/10/2108_10-phut-tap-yoga-cung-thuy-trang-2-e1max-1800x1800-2138.jpg\" style=\"height:533px; width:800px\" /></p>\r\n\r\n<p>Động t&aacute;c n&agrave;y gi&uacute;p k&eacute;o căng cơ h&ocirc;ng v&agrave; cơ giữa hai ch&acirc;n, cải thiện khả năng vận động của khớp h&ocirc;ng v&agrave; giảm đau trong chu kỳ kinh nguyệt.</p>\r\n\r\n<p><strong>Động t&aacute;c mở rộng hai ch&acirc;n</strong></p>\r\n\r\n<p>Ngồi thẳng, hai ch&acirc;n mở rộng hết mức c&oacute; thể, hai tay duỗi thẳng hướng về ph&iacute;a b&agrave;n ch&acirc;n, h&iacute;t s&acirc;u. Thở ra từ từ, hạ thấp người, c&uacute;i về ph&iacute;a trước. Lặp lại động t&aacute;c 8 &ndash; 10 lần trong 1 ph&uacute;t.</p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/10/5-2138.jpg\" style=\"height:400px; width:600px\" /></p>\r\n\r\n<p>Động t&aacute;c n&agrave;y gi&uacute;p giảm đau lưng, k&iacute;ch th&iacute;ch m&aacute;u lưu th&ocirc;ng ở xương chậu, cải thiện chức năng của buồng trứng, điều h&ograve;a chu kỳ kinh nguyệt.</p>\r\n\r\n<p><strong>Động t&aacute;c c&uacute;i gập người</strong></p>\r\n\r\n<p>Quỳ gối tr&ecirc;n thảm, m&ocirc;ng đặt l&ecirc;n tr&ecirc;n g&oacute;t ch&acirc;n, c&uacute;i gập người về ph&iacute;a trước sao cho ngực chạm v&agrave;o đ&ugrave;i, hai tay duỗi thẳng vươn l&ecirc;n ph&iacute;a trước c&agrave;ng xa c&agrave;ng tốt. Giữ tư thế trong 1 ph&uacute;t.</p>\r\n\r\n<p><img alt=\"dong-tac-giam-dau-lung-tieu-mo-2\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/10/dong-tac-giam-dau-lung-tieu-mo-2-2140.jpg\" style=\"height:400px; width:600px\" /></p>\r\n\r\n<p>Động t&aacute;c n&agrave;y gi&uacute;p thư gi&atilde;n v&ugrave;ng lưng dưới v&agrave; cổ, k&iacute;ch th&iacute;ch lưu th&ocirc;ng m&aacute;u ở v&ugrave;ng xương chậu nhỏ.</p>\r\n\r\n<p><strong>Động t&aacute;c c&uacute;i mặt</strong></p>\r\n\r\n<p>Đứng thẳng, từ từ hạ thấp phần th&acirc;n tr&ecirc;n, c&uacute;i người về ph&iacute;a trước, chống hai b&agrave;n tay xuống đất, giữ cho đầu gối thẳng. Giữ tư thế trong 30 gi&acirc;y, lặp lại 3 &ndash; 5 lần.</p>\r\n\r\n<p><img alt=\"Empty\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/10/dong-tac-yoga-giup-ban-tre-hon-6-2140.jpg\" style=\"height:599px; width:900px\" /></p>\r\n\r\n<p>Động t&aacute;c n&agrave;y gi&uacute;p tăng cường m&aacute;u lưu th&ocirc;ng l&ecirc;n n&atilde;o, k&eacute;o gi&atilde;n phần h&ocirc;ng, gi&uacute;p khu&ocirc;n mặt hồng h&agrave;o hơn.</p>\r\n\r\n<p><strong>Động t&aacute;c vũ c&ocirc;ng</strong></p>\r\n\r\n<p>Đứng thẳng, n&acirc;ng một ch&acirc;n về ph&iacute;a sau, tay nắm lấy mũi ch&acirc;n, từ từ c&uacute;i người về ph&iacute;a trước, tay c&ograve;n lại giơ l&ecirc;n cao, n&acirc;ng ch&acirc;n sau hết mức c&oacute; thể. Giữ tư thế trong 30 &ndash; 40 gi&acirc;y mỗi ch&acirc;n.</p>\r\n\r\n<p><img alt=\"tu-the-vu-cong\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/10/tu-the-vu-cong-2141.jpg\" style=\"height:921px; width:650px\" /></p>\r\n\r\n<p>Động t&aacute;c n&agrave;y gi&uacute;p chỉnh sửa tư thế, cải thiện chức năng thận v&agrave; tăng cường trao đổi chất.</p>\r\n\r\n<p>Nằm ngửa tr&ecirc;n thảm, hai đầu gối gập, hai tay duỗi thẳng, nắm lấy cổ ch&acirc;n. N&acirc;ng h&ocirc;ng v&agrave; lưng l&ecirc;n cao hết mức c&oacute; thể. Giữ tư thế trong 30 &ndash; 40 gi&acirc;y.</p>\r\n\r\n<p>Động t&aacute;c n&agrave;y gi&uacute;p giảm đau lưng, tăng cường cơ bụng, giảm mỡ v&ugrave;ng eo v&agrave; cải thiện ti&ecirc;u h&oacute;a.</p>\r\n\r\n<p><strong>Động t&aacute;c thư gi&atilde;n</strong></p>\r\n\r\n<p>Nằm ngửa tr&ecirc;n thảm, hai ch&acirc;n co lại, đầu gối mở, hai l&ograve;ng b&agrave;n ch&acirc;n &uacute;p v&agrave;o nhau, tay duỗi thẳng tự nhi&ecirc;n. Thư gi&atilde;n ho&agrave;n to&agrave;n trong 3 ph&uacute;t.</p>\r\n\r\n<p><img alt=\"muon-tre-lau-voc-dang-thon-gon-khong-kho-chi-can-bat-tay-luyen-tap-yoga-moi-ngay-12b06d\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/07/10/muon-tre-lau-voc-dang-thon-gon-khong-kho-chi-can-bat-tay-luyen-tap-yoga-moi-ngay-12b06d-2142.jpeg\" style=\"height:588px; width:680px\" /></p>\r\n\r\n<p>Mục ti&ecirc;u đẹp d&aacute;ng, đẹp da v&agrave; trẻ l&acirc;u thực sự kh&ocirc;ng qu&aacute; kh&oacute;; c&oacute; lẽ r&agrave;o cản duy nhất ch&uacute;ng ta phải vượt qua l&agrave; chiến thắng ch&iacute;nh bản th&acirc;n m&igrave;nh v&agrave; ki&ecirc;n tr&igrave; thực hiện những b&agrave;i tập yoga như tr&ecirc;n để tạo n&ecirc;n lối sống điều độ. Ngo&agrave;i ra, bạn cũng h&atilde;y đảm bảo chế độ ăn uống khoa học v&agrave; hợp l&yacute; để c&oacute; thể đạt hiệu quả tối đa nhất. Ch&uacute;c c&aacute;c bạn sớm c&oacute; được một diện mạo khỏe đẹp từ trong ra ngo&agrave;i nh&eacute;!</p>\r\n', 'img/news/5-dong-tac-danh-cho-nhung-nguoi-bi-mat-ngu-1-2138.jpg', '27/09/2019', 10),
('4 động tác yoga cơ bản giúp bạn gái có được vóc dáng săn chắc, gợi cảm', 'Các tư thế yoga sẽ là những bài tập cơ bản giúp bạn kéo căng cơ và giữ gìn vóc dáng thon gọn', '<p><strong>1. Tư thế con m&egrave;o</strong></p>\r\n\r\n<p>Tư thế con m&egrave;o l&agrave; một trong những động t&aacute;c yoga cơ bản v&agrave; dễ thực hiện. T&aacute;c dụng của ch&uacute;ng kh&ocirc;ng chỉ gi&uacute;p cơ bụng, h&ocirc;ng săn chắc m&agrave; c&ograve;n c&oacute; khả năng l&agrave;m k&eacute;o gi&atilde;n đốt sống lưng v&agrave; cổ, giải ph&oacute;ng năng lượng hiệu quả.</p>\r\n\r\n<p>- Quỳ gối, đặt hai tay xuống s&agrave;n. Chống hai tay song song v&agrave; khoảng c&aacute;ch bằng vai.</p>\r\n\r\n<p>- Cố định l&ograve;ng b&agrave;n tay thật chắc. Đầu gối v&agrave; ch&acirc;n mở rộng tr&ecirc;n một đường thẳng v&agrave; vu&ocirc;ng g&oacute;c với mặt s&agrave;n.</p>\r\n\r\n<p>- H&iacute;t v&agrave;o, nhấc cằm v&agrave; ngực nh&igrave;n về ph&iacute;a trước. Lưng cong xuống.</p>\r\n\r\n<p>- Từ từ thở ra c&uacute;i cằm về ph&iacute;a ngực. Lưng đẩy l&ecirc;n cao hết mức c&oacute; thể, siết cơ bụng v&agrave; h&ocirc;ng.</p>\r\n\r\n<p>- H&iacute;t thở s&acirc;u v&agrave; chậm, giữ tư thế trong v&agrave;i nhịp thở.</p>\r\n\r\n<p>- Từ từ thở ra v&agrave; trở lại tư thế ban đầu.</p>\r\n\r\n<p>- Thực hiện 5 - 6 lần.</p>\r\n\r\n<p><strong>2. Tư thế chiến binh</strong></p>\r\n\r\n<p>Với c&aacute;c động t&aacute;c khuỵu gối, n&acirc;ng cột sống, dang tay&hellip; của tư thế yoga chiến binh n&agrave;y kh&ocirc;ng chỉ gi&uacute;p bạn đốt mỡ ở v&ugrave;ng h&ocirc;ng, bụng m&agrave; c&ograve;n l&agrave;m săn chắc c&aacute;nh tay v&agrave; đ&ugrave;i. Đ&acirc;y được xem l&agrave; tư thế linh hoạt v&agrave; t&aacute;c động đồng đều đến to&agrave;n bộ cơ thể.</p>\r\n\r\n<p><img alt=\"con-meo\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/04/18/con-meo-1258.jpg\" style=\"height:399px; width:600px\" /><img alt=\"\" src=\"https://bs.serving-sys.com/serving/adServer.bs?cn=display&amp;c=19&amp;mc=imp&amp;pli=28970257&amp;PluID=0&amp;ord=1569591643721&amp;rtu=-1\" style=\"height:0px; width:0px\" /></p>\r\n\r\n<ul>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n</ul>\r\n\r\n<p>- Đứng dang rộng ch&acirc;n tr&ecirc;n thảm, lưng thẳng.</p>\r\n\r\n<p>- Xoay b&agrave;n ch&acirc;n tr&aacute;i 90 độ ra ngo&agrave;i, để g&oacute;t ch&acirc;n tr&aacute;i v&agrave; b&agrave;n ch&acirc;n phải nằm tr&ecirc;n một đường thẳng.</p>\r\n\r\n<p>- Dang rộng tay song song với mặt s&agrave;n.</p>\r\n\r\n<p>- Khuỵu gối ch&acirc;n tr&aacute;i sao cho vu&ocirc;ng g&oacute;c, giữ thẳng lưng v&agrave; siết chặt bắp tay.</p>\r\n\r\n<p>- H&iacute;t thở đều, giữ tư thế trong v&ograve;ng 5 - 10 gi&acirc;y.</p>\r\n\r\n<p>- Lặp lại v&agrave; đổi ch&acirc;n khoảng 5 - 6 lần.</p>\r\n\r\n<p><strong>3. Tư thế tấm v&aacute;n</strong></p>\r\n\r\n<p>Tư thế tấm v&aacute;n t&aacute;c động đến to&agrave;n bộ c&aacute;c bộ phận từ đầu, lưng, bụng, đ&ugrave;i v&agrave; tay gi&uacute;p đốt ch&aacute;y mỡ thừa hiệu quả. Ch&uacute;ng cũng g&oacute;p phần tăng cường sức mạnh cơ thể, giải ph&oacute;ng căng thẳng ở cổ cũng như k&eacute;o gi&atilde;n cột sống.</p>\r\n\r\n<p><img alt=\"5_1\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/04/18/5_1-1256.jpg\" style=\"height:433px; width:650px\" /></p>\r\n\r\n<p>- Bắt đầu bằng tư thế con m&egrave;o.</p>\r\n\r\n<p>- H&iacute;t v&agrave;o, ấn mạnh l&ograve;ng b&agrave;n tay xuống s&agrave;n v&agrave; duỗi thẳng ch&acirc;n, đầu gối.</p>\r\n\r\n<p>- Khuỷu tay thẳng, đ&ugrave;i h&ocirc;ng n&acirc;ng l&ecirc;n v&agrave; h&ocirc;ng kh&ocirc;ng được qu&aacute; thấp. Giữ đầu, cột sống v&agrave; ch&acirc;n thẳng h&agrave;ng tr&ecirc;n một đường thẳng.</p>\r\n\r\n<p>- Từ từ mở rộng vai tr&ecirc;n v&agrave; xương đ&ograve;n rộng. Ấn b&agrave;n ch&acirc;n v&agrave; l&ograve;ng b&agrave;n tay dang rộng để giữ thăng bằng.</p>\r\n\r\n<p>- Giữ tư thế h&iacute;t thở trong 10 gi&acirc;y.</p>\r\n\r\n<p><strong>4. Tư thế rắn hổ mang</strong></p>\r\n\r\n<p>Tư thế n&agrave;y t&aacute;c động tập trung v&agrave;o phần bụng dưới v&agrave; cơ đ&ugrave;i n&ecirc;n sẽ gi&uacute;p c&aacute;c cơ quan n&agrave;y săn chắc hơn. B&ecirc;n cạnh đ&oacute;, tư thế rắn hổ mang cực k&igrave; hiệu quả cho đốt sống lưng khỏe mạnh v&agrave; dẻo dai, căng cơ lưng v&agrave; bụng để tăng cường hệ ti&ecirc;u h&oacute;a, đồng thời gi&uacute;p lưu th&ocirc;ng m&aacute;u tốt.</p>\r\n\r\n<p><img alt=\"yoga-sex\" src=\"https://media.ex-cdn.com/EXP/media.phunutoday.vn/files/content/2019/04/18/yoga-sex-1257.jpg\" style=\"height:600px; width:900px\" /></p>\r\n\r\n<p>- Nằm sấp, tr&aacute;n chạm s&agrave;n v&agrave; ch&acirc;n duỗi thẳng, &uacute;p mu b&agrave;n ch&acirc;n xuống s&agrave;n.</p>\r\n\r\n<p>- Đặt b&agrave;n tay dưới vai v&agrave; ấn mũi ch&acirc;n, xương h&ocirc;ng xuống s&agrave;n.</p>\r\n\r\n<p>- H&iacute;t v&agrave;o, dồn trọng lực cơ thể l&ecirc;n hai l&ograve;ng b&agrave;n tay. Uốn cong lưng v&agrave; n&acirc;ng đầu, ngực ra khỏi s&agrave;n.</p>\r\n\r\n<p>- Hướng đầu ra ph&iacute;a sau, mắt nh&igrave;n l&ecirc;n trần nh&agrave;.</p>\r\n\r\n<p>- Giữ tư thế, h&iacute;t thở đều trong 8 - 10 gi&acirc;y.</p>\r\n\r\n<p>- Thở ra, chậm r&atilde;i hạ th&acirc;n trước v&agrave; đầu về tư thế ban đầu. Thả lỏng</p>\r\n', 'img/news/yoga-sex-1257.jpg', '27/09/2019', 11),
('Đẩy vai sau bằng tạ đòn có phải là bài tập nguy hiểm?', 'Bài tập đẩy vai sau bằng tạ đòn (Behind the neck press) là dạng bài tập được các vận động viên thể hình chuyên nghiệp xem trọng trong lộ trình tập luyện của mình. Nhưng hiện nay có thông tin cho răng ', '<h2>Đẩy vai sau bằng tạ đ&ograve;n (Behind the neck press)</h2>\r\n\r\n<p><img alt=\"\" src=\"https://swequity.vn/wp-content/uploads/2019/09/day-vai-sau-bang-ta-1024x576.jpg\" style=\"height:576px; width:1024px\" /></p>\r\n\r\n<p>Behind the neck press l&agrave; b&agrave;i tập vai nổi tiếng l&agrave;m n&ecirc;n t&ecirc;n tuổi của rất nhiều huyền thoại thể h&igrave;nh. Ở nước ta n&oacute; được gọi với nhiều t&ecirc;n kh&aacute;c nhau như đẩy tạ sau đầu, tập vai sau với thanh đ&ograve;n, đẩy vai sau bằng tạ đ&ograve;n&hellip; B&agrave;i tập n&agrave;y t&aacute;c động tới nh&oacute;m cơ bắp của vai trước, vai ngo&agrave;i, vai sau, phần lưng ph&iacute;a tr&ecirc;n, tay sau v&agrave; cơ lưng x&ocirc;.&nbsp;</p>\r\n\r\n<p>B&agrave;i tập vai n&agrave;y gi&uacute;p tăng cường sức mạnh v&agrave; x&acirc;y dựng cơ bắp. Đặc biệt, n&oacute; gi&uacute;p t&aacute;c động mạnh tới phần lưng tr&ecirc;n v&agrave; tay sau từ đ&oacute; gi&uacute;p cải thiện sức mạnh của th&acirc;n tr&ecirc;n, tăng cường sự linh hoạt cho vai. B&ecirc;n cạnh đ&oacute;, d&ugrave; cơ vai chỉ l&agrave; cơ nhỏ nhưng b&agrave;i tập n&agrave;y lại t&aacute;c động tới nhiều nh&oacute;m cơ kh&ocirc;ng thua g&igrave; b&agrave;i compound từ đ&oacute; sẽ gi&uacute;p cải thiện hoạt động của vai, tăng cường độ dẻo dai, giảm nguy cơ chấn thương trong qu&aacute; tr&igrave;nh luyện tập.&nbsp;</p>\r\n\r\n<h3>Vậy b&agrave;i tập n&agrave;y c&oacute; thực sự nguy hiểm với gymer hay kh&ocirc;ng?</h3>\r\n\r\n<p>B&agrave;i tập n&agrave;y g&acirc;y ra nhiều &aacute;p lực l&ecirc;n phần vai v&agrave; c&ugrave;ng cổ nếu vai của bạn kh&ocirc;ng đủ độ dẻo hoặc phải tập qu&aacute; nặng th&igrave; khả năng năng chấn thương l&agrave; kh&aacute; cao. Ngo&agrave;i ra, do đường đi của thanh tạ sẽ rơi về ph&iacute;a sau n&ecirc;n c&oacute; thể sẽ gặp phải chấn thương cổ nếu bạn kh&ocirc;ng tập đ&uacute;ng kỹ thuật.&nbsp;</p>\r\n\r\n<p>D&ugrave; b&agrave;i tập n&agrave;y mang nhiều lợi &iacute;ch nhưng vẫn được khuyến c&aacute;o l&agrave; kh&ocirc;ng n&ecirc;n tập. Đặc biệt, nước ta l&agrave; quốc gia mới ph&aacute;t triển lĩnh vực fitness n&agrave;y trong thời gian gần đ&acirc;y. Đối tượng tập l&agrave; học sinh, sinh vi&ecirc;n với lượng kiến thức kh&ocirc;ng nhiều, nếu kh&ocirc;ng c&oacute; sự chỉ dẫn v&agrave; theo d&otilde;i s&aacute;t sao của người c&oacute; chuy&ecirc;n m&ocirc;n th&igrave; thực sự l&agrave; kh&ocirc;ng an to&agrave;n.&nbsp;</p>\r\n\r\n<h2>C&oacute; n&ecirc;n tập behind the neck press hay kh&ocirc;ng?</h2>\r\n\r\n<p><img alt=\"\" src=\"https://swequity.vn/wp-content/uploads/2019/09/day-vai-sau-bang-ta-1-1024x576.jpg\" style=\"height:576px; width:1024px\" /></p>\r\n\r\n<p>Đ&acirc;y c&oacute; lẽ l&agrave; c&acirc;u hỏi nhiều người quan t&acirc;m, Swequity xin đưa ra một v&agrave;i gợi &yacute; như sau:&nbsp;</p>\r\n\r\n<p>Bạn c&oacute; thể tập nếu:&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Tập dưới sự gi&aacute;m s&aacute;t, hướng dẫn của người c&oacute; chuy&ecirc;n m&ocirc;n như c&aacute;c HLV tại Swequity.&nbsp;</li>\r\n	<li>Bạn đ&atilde; c&oacute; th&acirc;m ni&ecirc;n tập luyện v&agrave; c&oacute; thể tuwjkieemr so&aacute;t rủi ro khi tập luyện.&nbsp;</li>\r\n	<li>Tập luyện vai sau bằng thanh đ&ograve;n.&nbsp;</li>\r\n</ul>\r\n\r\n<p>Bạn kh&ocirc;ng n&ecirc;n tập nếu:&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Kh&ocirc;ng c&oacute; sự hướng dẫn của người c&oacute; chuy&ecirc;n m&ocirc;n</li>\r\n	<li>Bạn mắc c&aacute;c bệnh về cột sống, khớp, cổ</li>\r\n	<li>Đ&atilde; từng gặp c&aacute;c chấn thương về cổ, cột sống</li>\r\n	<li>Người lớn tuổi v&agrave; cơ vai kh&ocirc;ng thực sự dẻo dai</li>\r\n</ul>\r\n\r\n<p>Thực chất, b&agrave;i tập đẩy vai sau bằng tạ đ&ograve;n n&agrave;y kh&ocirc;ng qu&aacute; kh&oacute; nhưng trong qu&aacute; tr&igrave;nh tập bạn kh&ocirc;ng n&ecirc;n sử dụng mức tạ nặng v&igrave; c&oacute; thể sẽ mang tới rủi ro lớn. B&ecirc;n cạnh đ&oacute;, cần tuyệt đối tu&acirc;n thủ phương ph&aacute;p tập luyện, kh&ocirc;ng tự &yacute; thay đổi hoặc s&aacute;ng tạo th&ecirc;m c&aacute;ch tập.&nbsp;</p>\r\n\r\n<p>Hướng dẫn c&aacute;ch tập:&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Bước 1: Ngồi tr&ecirc;n ghế v&agrave; đặt ch&acirc;n l&ecirc;n s&agrave;n, đầu gối tạo với s&agrave;n 1 g&oacute;c 90 độ.&nbsp;</li>\r\n	<li>Bước 2: N&acirc;ng thanh tạ ra ph&iacute;a sau đầu (2 b&agrave;n tay cầm thanh tạ để rộng hơn vai). Với c&aacute;c bạn mới tập lần đầu h&atilde;y nhờ huấn luyện vi&ecirc;n n&acirc;ng thanh tạ l&ecirc;n v&agrave; đỡ thanh tạ xuống sau khi tập xong nh&eacute;).&nbsp;</li>\r\n	<li>Bước 3; N&acirc;ng thanh tạ l&ecirc;n v&agrave; thở ra. Lưu &yacute; giữ lưng thẳng, mắt nh&igrave;n về ph&iacute;a trước, n&acirc;ng thanh đ&ograve;n cao hơn đầu v&agrave; giữ nguy&ecirc;n trong 1s.&nbsp;</li>\r\n	<li>Bước 4: Từ từ hạ tạ về vị tr&iacute; cũ v&agrave; h&iacute;t v&agrave;o.&nbsp;</li>\r\n	<li>Bước 5: Thực hiện lặp lại động t&aacute;c từ 12 &ndash; 15 lần. Để kết th&uacute;c b&agrave;i tập, bạn n&acirc;ng thanh tạ l&ecirc;n v&agrave; hạ về ph&iacute;a trước thay v&igrave; ph&iacute;a sau nh&eacute;. H&atilde;y nhờ người trợ gi&uacute;p nếu chưa quen tập nh&eacute;.&nbsp;</li>\r\n</ul>\r\n\r\n<p>C&aacute;ch tập kh&aacute;c l&agrave; bạn c&oacute; thể tập tương tự như c&aacute;c bước tr&ecirc;n nhưng tập ở tư thế đứng. C&aacute;ch n&agrave;y sẽ gi&uacute;p bạn c&oacute; thể chủ động hơn trong việc n&acirc;ng tạ l&ecirc;n v&agrave; hạ tạ xuống khi kh&ocirc;ng c&oacute; người hỗ trợ. Với bạn mới tập lần đầu c&oacute; thể chỉ cần sử dụng thanh đ&ograve;n m&agrave; kh&ocirc;ng cần tạ, n&acirc;ng nhiều lần gi&uacute;p cơ thể h&igrave;nh th&agrave;nh cảm giảm, phương ph&aacute;p n&agrave;y rất quan trọng với c&aacute;c bạn body kh&ocirc;ng c&oacute; độ dẻo. Ch&uacute;c c&aacute;c bạn th&agrave;nh c&ocirc;ng.</p>\r\n', 'img/news/day-vai-sau-bang-ta-1024x576.jpg', '27/09/2019', 12),
('Giải đáp vấn đề mới tập gym nên hay không nên tập ngực dưới?', 'Ngực là nhóm cơ nam giới luôn đặc biệt chú trọng khi tới phòng gym. Nhắc tới tập ngực cũng có rất nhiều vấn đề xung quanh, vấn đề gây tranh cãi nhiều nhất đó là tập ngực dưới. Chắc chắn bạn đã từng ng', '<h2>Mới tập gym n&ecirc;n hay kh&ocirc;ng n&ecirc;n tập ngực dưới?</h2>\r\n\r\n<p><img alt=\"\" src=\"https://swequity.vn/wp-content/uploads/2019/09/tap-nguc-duoi-1024x683.jpg\" style=\"height:683px; width:1024px\" /></p>\r\n\r\n<h3><strong>Thứ 1: Do cơ ngực chưa ph&aacute;t triển&nbsp;</strong></h3>\r\n\r\n<p>Đ&acirc;y l&agrave; l&yacute; do đầu ti&ecirc;n mọi người nhắc tới: H&atilde;y để ngực ph&aacute;t triển to rồi h&atilde;ng tập ngực dưới. Điều n&agrave;y l&agrave; do d&acirc;n tập gym thường chia ngực th&agrave;nh c&aacute;c phần như tr&ecirc;n, giữa, dưới, trong v&agrave; ngo&agrave;i. Nhưng thực tế th&igrave; c&aacute;c b&agrave;i tập ngực sẽ t&aacute;c động tới to&agrave;n bộ ngực cũng như c&aacute;c nh&oacute;m cơ li&ecirc;n quan (tay sau, vai).&nbsp;</p>\r\n\r\n<p>C&aacute;c b&agrave;i tập ngực dưới thường được sử dụng như đẩy tạ với ghế dốc ngược (decline Bench Press), Dips&hellip; những b&agrave;i tập n&agrave;y t&aacute;c động l&ecirc;n ngực kh&ocirc;ng nhiều. V&igrave; vậy, để tối ưu lực t&aacute;c động v&agrave; hiệu quả của buổi tập ngực, hầu hết mọi người đều &iacute;t khi sử dụng c&aacute;c b&agrave;i tập ngực dưới. Thay v&agrave;o đ&oacute; l&agrave; b&agrave;i tập với ghế phẳng, ghế dốc (cho ngực giữa v&agrave; tr&ecirc;n) gi&uacute;p người mới ph&aacute;t triển được bộ ngực trước rồi mới đi đến ngực dưới.</p>\r\n\r\n<p>Cơ ngực thường c&oacute; khoảng thời gian ph&aacute;t triển l&agrave; từ 2 &ndash; 4 th&aacute;ng, h&atilde;y tập về kết hợp với chế độ dinh dưỡng ph&ugrave; hợp sau đ&oacute; mới tập để ph&aacute;t triển cơ ngực dưới nh&eacute;.&nbsp;</p>\r\n\r\n<h3><strong>Thứ 2: Người mới thường kh&ocirc;ng cảm nhận được cơ ngực dưới</strong></h3>\r\n\r\n<p>Như ở tr&ecirc;n c&oacute; n&oacute;i, b&agrave;i tập ph&aacute;t triển cơ ngực dưới l&agrave; những b&agrave;i sẽ &iacute;t ảnh hưởng tới to&agrave;n bộ phần ngực. Nếu bạn chưa biết c&aacute;ch cảm nhận cơ bắp khi tập luyện th&igrave; sẽ khiến hiệu quả giảm xuống.</p>\r\n\r\n<h3><strong>Thứ 3: Chưa ph&acirc;n phối được thời gian tập luyện</strong></h3>\r\n\r\n<p>Một yếu tố quan trọng quyết định việc người mới kh&ocirc;ng n&ecirc;n tập cơ ngực dưới đ&oacute; ch&iacute;nh l&agrave; thời gian của buổi tập. C&aacute;c newbie thường c&oacute; th&oacute;i quen tập theo sở th&iacute;ch của bản th&acirc;n. Bạn n&agrave;o si&ecirc;ng th&igrave; tập tới kiệt sức, bạn n&agrave;o lười th&igrave; vừa tập vừa chơi, điều n&agrave;y sẽ g&acirc;y ảnh hưởng lớn tới kết quả tập luyện.&nbsp;</p>\r\n\r\n<p>Thời gian tập luyện của người mới k&eacute;o d&agrave;i kh&ocirc;ng qu&aacute; 1 giờ. C&aacute;c b&agrave;i tập với số set, reps v&agrave; thời gian nghỉ giữa c&aacute;c set cần phải ph&acirc;n bố ph&ugrave; hợp. V&igrave; vậy, c&aacute;c HLV sẽ ưu ti&ecirc;n những b&agrave;i tập hiệu quả nhất trong từng giai đoạn. Ở c&aacute;c bạn mới tập (từ 1 &ndash; 3 th&aacute;ng) sẽ l&agrave; c&aacute;c b&agrave;i tập compound như incline bench press hay bench press. Khi bạn đ&atilde; tập c&oacute; th&acirc;m ni&ecirc;n th&igrave; h&atilde;y thay đổi c&aacute;c b&agrave;i tập, bổ sung c&aacute;c b&agrave;i tập ngực dưới để ho&agrave;n thiện cơ ngực nh&eacute;.</p>\r\n\r\n<p><img alt=\"\" src=\"https://swequity.vn/wp-content/uploads/2019/09/tap-nguc-duoi-1.jpg\" style=\"height:647px; width:970px\" /></p>\r\n\r\n<h3><strong>Thứ 4: Chưa đ&aacute;p ứng được chế độ tập luyện, dinh dưỡng, v&agrave; nghỉ ngơi</strong></h3>\r\n\r\n<p>Đ&acirc;y l&agrave; nguy&ecirc;n nh&acirc;n quan trọng nhất nhưng nhiều bạn trẻ lại kh&ocirc;ng ch&uacute; &yacute;. Với tập ngực dưới n&oacute;i ri&ecirc;ng v&agrave; tập c&aacute;c nh&oacute;m cơ kh&aacute;c n&oacute;i chung th&igrave; bạn cần phải đầu tư thời gian v&agrave; ti&ecirc;u tốn năng lượng cho buổi tập. Giải th&iacute;ch dễ hiểu hơn l&agrave; khi tập ngực dưới kh&ocirc;ng c&oacute; nghĩa l&agrave; bạn bỏ qua c&aacute;c b&agrave;i tập kh&aacute;c m&agrave; ngược lại bạn cần phải th&ecirc;m v&agrave;o c&aacute;c b&agrave;i tập ri&ecirc;ng cho ngực dưới.&nbsp;</p>\r\n\r\n<p>Như vậy, c&aacute;c buổi tập của bạn sẽ trở n&ecirc;n nặng hơn, cần nhiều thời gian hơn v&agrave; mất nhiều năng lượng hơn. Nhưng hầu hết những gymer mới đến ph&ograve;ng tập th&igrave; lại kh&ocirc;ng ăn uống đầy đủ dinh dưỡng cho c&aacute;c buổi tập.&nbsp;</p>\r\n\r\n<p>V&igrave; vậy, thay v&igrave; tạo ra một buổi tập vất vả với h&agrave;ng đống b&agrave;i tập cho ngực tr&ecirc;n, ngực giữa, ngực dưới, trong ngo&agrave;i&hellip; H&atilde;y bắt đầu chậm r&atilde;i từ c&aacute;c b&agrave;i compound hiệu quả v&agrave; rồi gia tăng theo sự ph&aacute;t triển của cơ thể nh&eacute;!</p>\r\n\r\n<h3><strong>Thứ 5: Do mục đ&iacute;ch tập luyện kh&ocirc;ng ph&ugrave; hợp</strong></h3>\r\n\r\n<p>Thực sự &iacute;t ai hiểu đươc &yacute; nghĩa của việc tập ngực dưới. Ch&uacute;ng ta thường m&ocirc; phỏng theo những g&igrave; ch&uacute;ng ta thấy thay v&igrave; t&igrave;m hiểu. Đối với một người mới bạn c&oacute; thể hiểu đơn giản như sau: c&aacute;c nh&oacute;m cơ thường được chia l&agrave;m nhiều phần (tr&ecirc;n, giữa, dưới, trong, ngo&agrave;i&hellip;). V&iacute; dụ ngực sẽ c&oacute; ngực tr&ecirc;n, ngực giữa, ngực dưới hay vai sẽ c&oacute; vai trước, vai sau, đ&ugrave;i sẽ c&oacute; đ&ugrave;i trước, đ&ugrave;i sau&hellip;</p>\r\n\r\n<h3>Vậy người mới tập gym c&oacute; n&ecirc;n tập ngực dưới kh&ocirc;ng?</h3>\r\n\r\n<p>Bạn tập ngực dưới hay bất k&igrave; nh&oacute;m cơ n&agrave;o để đạt được hiệu quả cần c&oacute; sự hợp l&yacute; trong chế độ tập luyện, dinh dưỡng v&agrave; nghỉ ngơi. Nếu bạn đ&aacute;p ứng được những yếu tố tr&ecirc;n th&igrave; d&ugrave; l&agrave; người mới bạn vẫn c&oacute; thể tập ngực dưới hay nh&oacute;m cơ n&agrave;o bạn th&iacute;ch</p>\r\n', 'img/news/tap-nguc-duoi.jpg', '27/09/2019', 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

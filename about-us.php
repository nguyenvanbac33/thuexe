<?php
include 'header.php';
?>
<div class="about-us-area pt-125 pb-125">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="overview-content">
                    <h1><span>Dịch vụ cho thuê xe</span></h1>
                    <h2>Khu du lịch Đà Nẵng</h2>
                    <p>Chúng tôi hiểu chất lượng xe đóng vai trò rất quan trọng ảnh hưởng đến chuyến đi của bạn, nên tất cả các xe máy cho thuê của chúng tôi là xe được mua mới 100%. Ngoài ra, toàn bộ các xe sau khi kết thúc hợp đồng với khách sẽ được kiểm tra, bảo dưỡng, thay thế các bộ phận hỏng hóc và phải đạt chuẩn an toàn xe trước khi giao cho khách hàng mới.</p>
                    <div class="question-area">
                        <h4>Liên hệ? </h4>
                        <div class="question-contact">
                            <div class="question-icon">
                                <i class="icofont icofont-phone"></i>
                            </div>
                            <div class="question-content-number">
                                <h6> 01245 658 698</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="overview-img">
                    <img class="tilter" src="assets/img/banner/banner-1.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="services-area pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-services orange mb-30 text-center">
                    <div class="services-icon">
                        <img alt="" src="assets/img/icon-img/3.png">
                    </div>
                    <div class="services-text">
                        <h5>GIAO XE TẬN NƠI</h5>
                        <p>Áp dụng cho toàn bộ đơn đặt xe</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-services yellow mb-30 text-center">
                    <div class="services-icon">
                        <img alt="" src="assets/img/icon-img/4.png">
                    </div>
                    <div class="services-text">
                        <h5>ONLINE SUPPORT</h5>
                        <p>Support 24/24 mọi vấn đề</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-services purple mb-30 text-center">
                    <div class="services-icon">
                        <img alt="" src="assets/img/icon-img/5.png">
                    </div>
                    <div class="services-text">
                        <h5>MONEY RETURN</h5>
                        <p>Hoàn tiền khi không hài lòng</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-services sky mb-30 text-center">
                    <div class="services-icon">
                        <img alt="" src="assets/img/icon-img/6.png">
                    </div>
                    <div class="services-text">
                        <h5>MEMBER DISCOUNT</h5>
                        <p>Giảm giá khi thuê nhiều</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
?>
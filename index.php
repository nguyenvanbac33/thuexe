<?php
include 'header.php';
?>
<div class="overview-area pt-135">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="overview-content">
                    <h1><span>ĐỒ THỂ THAO</span></h1>
                    <h2>Yoga Collection</h2>
                    <p>Bộ sưu tập đồ tập Yoga 2017 chất lượng với nhiều mẫu mã trẻ trung, kiểu dáng thời trang thể thao mới nhất</p>
                    <div class="question-area">
                        <h4>Liên hệ? </h4>
                        <div class="question-contact">
                            <div class="question-icon">
                                <i class="icofont icofont-phone"></i>
                            </div>
                            <div class="question-content-number">
                                <h6> 01245 658 698</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="overview-img">
                    <img class="tilter" src="assets/img/banner/quan-ao-tap-gym-nu-dep.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="banner-area pt-135 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="banner-wrapper mb-30">
                    <a href="#"><img src="assets/img/banner/ao-thun-the-thao-f5-trang-4-600x600.jpg" width="100%" height="300" alt="image"></a>
                    <div class="banner-content">
                        <h2>GIAO HÀNG MIỄN PHÍ</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="banner-wrapper mb-30">
                    <a href="#"><img height="300" width="100%" src="assets/img/banner/do-tap-gym-banner-nho.jpg" alt="image"></a>
                    <div class="banner-content">
                        <h2>HỖ TRỢ IN ẤN</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="banner-wrapper mb-30">
                    <a href="#"><img src="assets/img/banner/tennis-1.jpg" width="100%" height="300" alt="image"></a>
                    <div class="banner-content">
                        <h2>HỖ TRỢ ĐỔI TRẢ</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blog-area pt-150 pb-110">
    <div class="container">
        <div class="section-title text-center mb-60">
            <h2>SẢN PHẨM ƯU CHUỘNG</h2>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="grid-list-product-wrapper tab-content">
                    <div id="new-product" class="product-grid product-view tab-pane active">
                        <div class="row">
                            <?php
                            $sql = "SELECT *, (SELECT url FROM image where a.id = id_item limit 1) as image FROM item a ORDER BY RAND() limit 9";
                            include 'item.php';
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
?>
<?php
include "header.php";
$id = $_GET['id'];
$sql = "SELECT a.*, b.name as group_name FROM item a inner join `group` b on a.id_group = b.id where a.id = $id";
$query = $conn -> query($sql);
$row = $query -> fetch_array();
?>
<div class="product-details-area fluid-padding-3 ptb-130">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="product-details-img-content">
                    <div class="product-details-tab mr-40">
                        <div class="product-details-large tab-content">
                            <?php
                            $count = 0;
                            $sql = "SELECT * FROM image where id_item = $id";
                            $query = $conn -> query($sql);
                            while ($row1 = $query -> fetch_array()) {
                                $active = '';
                                if ($count == 0) {
                                    $active = 'active';
                                }
                                $count++;
                                $id_image = $row1['id'];
                                $url = $row1['url'];
                                ?>
                                <div class="tab-pane <?php echo $active?>" id="<?php echo $id_image?>">
                                    <div class="easyzoom easyzoom--overlay">
                                        <a href="<?php echo $url?>">
                                            <img width="600" height="500" src="<?php echo $url?>" alt="">
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="product-details-small nav mt-12 product-dec-slider owl-carousel">
                            <?php
                            $sql = "SELECT * FROM image where id_item = $id";
                            $query = $conn -> query($sql);
                            $count = 0;
                            while ($row1 = $query -> fetch_array()) {
                                $active = '';
                                if ($count == 0) {
                                    $active = 'class="active"';
                                }
                                $count++;
                                $id_image = $row1['id'];
                                $url = $row1['url'];
                                ?>
                                <a <?php echo $active?> href="#<?php echo $id_image?>">
                                    <img height="200" src="<?php echo $url?>" alt="">
                                </a>
                                <?php
                            }
                            ?>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="product-details-content">
                    <h2><?php echo $row['name']?></h2>
                    <!-- <div class="quick-view-rating">
                        <i class="fa fa-star reting-color"></i>
                        <i class="fa fa-star reting-color"></i>
                        <i class="fa fa-star reting-color"></i>
                        <i class="fa fa-star reting-color"></i>
                        <i class="fa fa-star reting-color"></i>
                        <span> ( 01 Customer Review )</span>
                    </div> -->
                    <div class="product-overview">
                        <h5 class="pd-sub-title">Loại xe</h5>
                        <p><?php echo $row['group_name']?></p>
                    </div>
                    <div class="product-overview">
                        <h5 class="pd-sub-title">Hãng xe</h5>
                        <p><?php echo $row['producer']?></p>
                    </div>
                    <div class="product-price">
                        <span><?php echo formatPrice($row['price'])?></span>
                    </div>
                    <form method="post" action="?id=<?php echo $id?>">
                        <select name="size" style="margin-top: 20px">
                            <option value="S">S</option>
                            <option value="M">M</option>
                            <option value="L">L</option>
                            <option value="XL">XL</option>
                            <option value="XXL">XXL</option>
                            <option value="XXXL">XXXL</option>
                        </select>
                        <div class="quickview-plus-minus">
                            <div class="cart-plus-minus" style="width: 200px">
                                <input style="width: 120px" type="number" value="1" min="1" name="count"  class="cart-plus-minus-box">
                            </div>
                            <div class="quickview-btn-cart">
                                <button type="submit" name="add-cart" class="btn btn-style cr-btn" style="height: 100%"><span>add to cart</span></button>
                            </div>
                        </div>
                    </form>
                    <div class="product-share">
                        <h5 class="pd-sub-title">Share</h5>
                        <ul>
                            <li>
                                <a href="#"><i class="icofont icofont-social-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="icofont icofont-social-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="icofont icofont-social-pinterest"></i></a>
                            </li>
                            <li>
                                <a href="#"> <i class="icofont icofont-social-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-overview" style="padding: 100px">
            <h3 class="pd-sub-title" style="margin-top: 50px">Mô tả</h3>
            <p><?php echo $row['desc']?></p>
        </div>
    </div>
</div>

<div class="pb-130">
    <div class="row" style="justify-content: center;">
        <div class="overview-content">
            <h1><span>SẢN PHẨM KHÁC</span></h1>
            <br/>
        </div>
        <div class="col-lg-12">
            <div class="row" style="justify-content: center;">
                <?php
                $sql = "SELECT *, (SELECT url FROM image where a.id = id_item limit 1) as image FROM item a ORDER BY RAND() limit 5";
                $query = $conn -> query($sql);
                while ($row = $query -> fetch_array()) {
                    ?>
                    <div class="col-lg-2 col-md-2">
                        <a href="item-details.php?id=<?php echo $row['id']?>">
                            <div class="blog-hm-wrapper mb-40">
                                <div class="blog-img">
                                    <img height="250" src="<?php echo $row['image']?>" alt="image">
                                </div>
                                <div>
                                    <center>
                                        <h3><?php echo $row['name']?></h3>
                                        <p><?php echo $row['price']?></p>
                                    </center>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
include 'footer.php';
?>
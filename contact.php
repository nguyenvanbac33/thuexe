<?php
include 'header.php';
?>
<div class="container" style="margin-top: 20px">
    <div class="row">
        <div class="col-lg-6"  style="padding: 0px">
            <div class="contact-info-wrapper">
                <div class="communication-info" style="margin-right: 0px">
                    <div class="single-communication">
                        <div class="communication-icon">
                            <i class="ti-home" aria-hidden="true"></i>
                        </div>
                        <div class="communication-text">
                            <h4>Address:</h4>
                            <p>Miata 309 S Main St,Stafford, KS 67578</p>
                        </div>
                    </div>
                    <div class="single-communication">
                        <div class="communication-icon">
                            <i class="ti-mobile" aria-hidden="true"></i>
                        </div>
                        <div class="communication-text">
                            <h4>Phone:</h4>
                            <p>0123 456 789 - 15 2368 4597</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6" style="padding: 0px">
            <div class="contact-info-wrapper">
                <div class="communication-info" style="margin-right: 0px">
                    <div class="single-communication">
                        <div class="communication-icon">
                            <i class="ti-email" aria-hidden="true"></i>
                        </div>
                        <div class="communication-text">
                            <h4>Email:</h4>
                            <p><a href="#">Support@BootExperts.com</a></p>
                        </div>
                    </div>
                    <div class="single-communication">
                        <div class="communication-icon">
                            <i class="ti-world" aria-hidden="true"></i>
                        </div>
                        <div class="communication-text">
                            <h4>Website:</h4>
                            <p><a href="#">https://DevItems.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12"  style="padding: 0px; margin-top: 20px">
            <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=%C4%90%C3%A0%20n%E1%BA%B5ng%20vi%E1%BB%87t%20nam&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </div>
    </div>
</div>
<?php
include 'footer.php';
?>